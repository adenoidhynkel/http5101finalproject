﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Content-Page.aspx.cs" Inherits="webApplicationDevelopmentFinal.Content_Page" %>
<asp:Content ID="PageContent" ContentPlaceHolderID="MainContent" runat="server">
<asp:SqlDataSource runat="server" id="generate_page" ConnectionString="<%$ ConnectionStrings:final_proj_db %>"></asp:SqlDataSource>
<h1 id="page_title" runat="server"></h1>
<div id="author_name" runat="server"></div>
<div id="publish_date" runat="server"></div>
<div id="result" runat="server"></div>
<div id="userOptions" runat="server">
    <a href="#" runat="server" id="editPageLink" class="btn btn-xs btn-default">Edit Page</a>
    <asp:Button ID="changePublishStatus" runat="server" Text="Publish Page" OnClick="Publish_Page" Class="btn btn-xs btn-primary"/>
    <asp:Button ID="btnDeletePage" runat="server" Text="Delete Page" OnClick="Delete_Page" Class="btn btn-xs btn-danger" />
</div>
<div id="page_content" runat="server"></div>
</asp:Content>
