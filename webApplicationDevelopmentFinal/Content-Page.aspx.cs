﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

// I used c#'s String.Format to concatenate the values in the string
// Reference: https://docs.microsoft.com/en-us/dotnet/api/system.string.format?view=netframework-4.7.2
namespace webApplicationDevelopmentFinal
{
    public partial class Content_Page : System.Web.UI.Page
    {
        public string findPage = "SELECT page_id, page_title, page_content, users_login_name, publish_status, publish_date FROM pages p JOIN users u ON p.author_id = u.users_id WHERE page_id = {0}";
        public string publishPage = "UPDATE pages SET publish_status = '{0}'";
        public string deleteContent = "DELETE FROM pages WHERE page_id={0}";
        public string successMessage;
        public string exceptionGenerated;
        public string pageRedirect = "Default.aspx?pagetitle={0}&deleted=true";

        // generate dismissable button and back link for the alert feature
        public string dismissButton = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\"> &times;</span></button>";
        public string backToHome = " <a href=\"Default.aspx\">Click Here to go back to pages</a>";


        // I took this from christine's code
        private string pageid
        {
            get { return Request.QueryString["pageid"]; }
        }


        // reference for storing sessions https://www.guru99.com/asp-net-session-management.html
        // I based this on Christine's query string code

        // retrieves login status stored in session 
        private string login
        {
            // cast session to string data type
            get { return (string)Session["login"]; }
            set { Session["login"] = value; }
        }
        // retrieves userid stored in session
        private int userid
        {
            // cast session to int data type
            get { return (int)Session["userid"]; }
            set { Session["userid"] = value; }
        }

        // retrieves username stored in session
        private string username
        {
            // cast session to string data type
            get { return (string)Session["username"]; }
        }

        // retrieves roleid stored in session
        private int roleid
        {
            // cast session to int data type
            get { return (int)Session["roleid"]; }
            set { Session["roleid"] = value; }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                // check if user is logged in, otherwise redirect to login page
                if (login == "" || login == null)
                {
                    login = "failed";
                    roleid = 0;
                    userid = 0;
                    Response.Redirect("Login-Form.aspx");
                }
                else if (login.Equals("failed") == true || login.Equals("loggedout") == true)
                {
                    // show the page but remove the user option if the user in not logged in
                    userOptions.InnerHtml = "";
                }

                // I took this if statement from christine's code
                string errorMsg = "";
                string statusTxt = "Are you sure with this action?";
                string datePublished;
                if (pageid == "" || pageid == null)
                {
                    page_title.InnerHtml = "Page not found";
                    page_content.InnerHtml = "please go back and try another";

                    // hide the buttons if the page has no content
                    userOptions.Visible = false;
                    return;
                }
                else
                {
                    // concatenate using string.format
                    findPage = String.Format(findPage, pageid);
                    using (SqlConnection showPage = new SqlConnection(generate_page.ConnectionString))
                    {
                        SqlCommand getPage = new SqlCommand(findPage, showPage);
                        try
                        {
                            getPage.Connection.Open();
                            SqlDataReader scanPage = getPage.ExecuteReader();
                            if (scanPage.Read() == true)
                            {
                                // set the edit page link
                                editPageLink.HRef = String.Format("Update-Page.aspx?pageid={0}", scanPage[0]);

                                // if data exists, show the content
                                page_title.InnerHtml = scanPage[1].ToString();
                                page_content.InnerHtml = scanPage[2].ToString();
                                datePublished = scanPage[5].ToString().Equals("") == true ? "Not yet published" : scanPage[5].ToString();
                                author_name.InnerHtml = String.Format("<span> Created by: {0} </span>", scanPage[3].ToString());
                                publish_date.InnerHtml = String.Format("<span> Date Published: {0} </span>", datePublished);

                                // Manipulate button text
                                changePublishStatus.Text = scanPage[4].ToString().Equals("published") == true ? "Unpublish Page" : "Publish Page";                          

                                // set clientside confirmation
                                // I got this from christine's code
                                changePublishStatus.OnClientClick = String.Format("if(!confirm('{0}')) return false", statusTxt);
                                btnDeletePage.OnClientClick = String.Format("if(!confirm('{0}')) return false", statusTxt);
                            }
                            else
                            {
                                // if data is not found show this instead
                                page_title.InnerHtml = "Page not found";
                                page_content.InnerHtml = "please go back and try another";
                            }
                        }
                        catch (Exception errMsg)
                        {
                            //store the error message
                            errorMsg += errMsg.Message;
                        }
                        finally
                        {
                            // close the connection regardless
                            showPage.Close();
                        }
                    }
                }

                if (errorMsg.Equals("") == false)
                {
                    result.InnerHtml = String.Format("<p class=\"alert alert-danger\"> {0} {1}</p>", errorMsg, backToHome);
                }
            }
        }

        protected void Publish_Page(object sender, EventArgs e)
        {
            string publishText = changePublishStatus.Text;
            if (publishText.Equals("Publish Page") == true)
            {
                publishPage = String.Format(publishPage, "published");
                publishPage += ",publish_date = SYSDATETIME()";
                // set the text to unpublish
                changePublishStatus.Text = "Unpublish Page";
                // set the message
                successMessage = String.Format("<p class=\"alert alert-success alert-dismissable\"> {0} was successfully published! {1} {2}</p>", page_title.InnerText, backToHome, dismissButton);
            }
            else
            {
                publishPage = String.Format(publishPage, "unpublished");
                // set the text to publish
                changePublishStatus.Text = "Publish Page";
                // set the message
                successMessage = String.Format("<p class=\"alert alert-info alert-dismissable\">{0} was successfully unpublished! {1} {2} </p>", page_title.InnerText, backToHome, dismissButton);
            }

            publishPage += String.Format(", last_edit_date = SYSDATETIME() WHERE page_id={0};", pageid);

            try
            {
                // I got this code from Christine
                generate_page.UpdateCommand = publishPage;
                generate_page.Update();
                result.InnerHtml = successMessage;
            }
            catch (Exception ex)
            {
                exceptionGenerated = String.Format("<p class=\"alert alert-danger alert-dismissable\"> {0} {1}</p>", ex.Message, dismissButton);
                result.InnerHtml = exceptionGenerated;
            }
        }

        protected void Delete_Page(object sender, EventArgs e)
        {
            try
            {
                // concatenate values using string format
                deleteContent = String.Format(deleteContent, pageid);

                // I got this code from christine
                generate_page.DeleteCommand = deleteContent;
                generate_page.Delete();

                // redirect the page with Querystrings
                pageRedirect = String.Format(pageRedirect, page_title.InnerText.ToString());
                Response.Redirect(pageRedirect);              
            }
            catch (Exception ex)
            {
                exceptionGenerated = String.Format("<p class=\"alert alert-danger alert-dismissable\"> {0} {1} {2}</p>", ex.Message , backToHome , dismissButton);
                result.InnerHtml = exceptionGenerated;
            }
        }
    }
}