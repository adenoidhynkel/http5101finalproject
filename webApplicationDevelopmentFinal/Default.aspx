﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="webApplicationDevelopmentFinal._Default" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
<asp:SqlDataSource runat="server" id="show_page" ConnectionString="<%$ ConnectionStrings:final_proj_db %>"></asp:SqlDataSource>
<!-- References for the bootstrap 
badges, pagination and tabbed panel
    https://getbootstrap.com/docs/3.3/components/#badges
    https://getbootstrap.com/docs/3.3/javascript/#tabs
-->
<h1>View Pages</h1>
 <a class="btn btn-primary pull-right" href="create-page.aspx"><span class="glyphicon glyphicon-plus"></span> Create a New Page</a>
 <div id="result" runat="server" class="result"></div>
<div>
  <!-- Nav tabs -->
  <ul class="nav nav-pills" role="tablist">
    <li role="presentation" class="active"><a href="#published" aria-controls="published" role="tab" data-toggle="tab">Published <span class="badge" id="publishedCount" runat="server">0</span></a></li>
    <li role="presentation"><a href="#unpublished" aria-controls="unpublished" role="tab" data-toggle="tab">Unpublished <span class="badge" id="unpublishedCount" runat="server">0</span></a></li>
  </ul>
  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="published">
        <div id="publishedContent" runat="server"></div>
    </div>
    <div role="tabpanel" class="tab-pane" id="unpublished">
        <div id="unpublishedContent" runat="server"></div>
    </div>
  </div>
</div>
</asp:Content>
