﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

namespace webApplicationDevelopmentFinal
{
    public partial class Create_Page : System.Web.UI.Page
    {
        public string titlePage;
        public string contentPage;
        public string publishString;
        public string publishDate;
        public bool publishStatus;
        public int menuStatus;
        public int authorID;

        // reference for storing sessions https://www.guru99.com/asp-net-session-management.html
        // I based this on Christine's query string code

        // retrieves login status stored in session 
        private string login
        {
            // cast session to string data type
            get { return (string)Session["login"]; }
            set { Session["login"] = value; }
        }

        // retrieves userid stored in session
        private int userid
        {
            // cast session to int data type
            get { return (int)Session["userid"]; }
        }

        // retrieves username stored in session
        private string username
        {
            // cast session to string data type
            get { return (string)Session["username"]; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            // check if user is logged in, otherwise redirect to login page
            if (login == "" || login == null)
            {
                login = "failed";
                Response.Redirect("Login-Form.aspx");
            }

            if (username != "" || username != null)
            {
                pageAuthor.Text = username;
            }
        }

        protected void Submit_Data(object sender, EventArgs e)
        {
            titlePage = pageTitle.Text;
            contentPage = pageContent.Text;
            authorID = userid; // gets the userid from the session
            
            // ternary operators to store the value

            //publish later
            publishStatus = chkPublishLater.Checked == true ? true : false; 

            // check to put in menu
            menuStatus = chkAddInMenu.Checked == true ? 1 : 0;

            // assign published or unpublish
            publishString = publishStatus == true ? "unpublished" : "published";

            // if it's set to publish later, do not put the publish date yet
            publishDate = publishStatus == true ? "NULL" : "SYSDATETIME()";
            


            // escape aposthropes to avoid errors
            // the string 'Mark's' would throw an error if
            // it is not escaped because it will detect it as an end of the string
            // and it will be confused so we will use asp's .replace
            // function to remedy this - 'Mark's' will become 'Mark''s'
            // I am sure there are a lot more elegant options in doing this
            // because this might present a bug later on...
            titlePage = titlePage.Replace("'", "''");
            contentPage = contentPage.Replace("'", "''");

            // I use String.Format for easier string concatenation
            // reference https://docs.microsoft.com/en-us/dotnet/api/system.string.format?view=netframework-4.7.2

            string insertQuery = String.Format("INSERT INTO pages(page_title, page_content, author_id, publish_status, publish_date, page_in_menu) VALUES ('{0}', '{1}', '{2}', '{3}', {4}, {5});", titlePage, contentPage, authorID, publishString, publishDate, menuStatus);
            string checkSelect = String.Format("SELECT LOWER(page_title) FROM pages WHERE LOWER(page_title) = '{0}';", titlePage.ToLower());
            string retrieveMAX = "SELECT MAX(page_id) FROM pages;";
            string dismissButton = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\"> &times;</span></button>";
            string exceptionGenerated = "";
            string successMessage = "";
            bool isDuplicateTitle = true;
            bool pageCreated = false;

            // references for using auto increment for mssql
            // https://chartio.com/resources/tutorials/how-to-define-an-auto-increment-primary-key-in-sql-server/

            // Executing a sql command that returns one value was possible when I used VB.net, I just thought that it would be 
            // possible in ASP.net as well because they both belong to microsoft   
            // references for the SQL Command
            // https://docs.microsoft.com/en-us/dotnet/api/system.data.sqlclient.sqlcommand.connection?view=netframework-4.7.2

            // I've worked with try-catch-finally blocks before in java in manipulating .txt files that is basically crud
            // and I thought I could use it for database crud because I can catch an exception or error
            // thrown by the database that might not be generated by asp.
            // here's my reference for the try-catch for asp
            // https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/keywords/try-catch-finally
            // https://stackify.com/csharp-exception-handling-best-practices/

            // check for server side validation
            // search for existing page titles
            using (SqlConnection sqlConn = new SqlConnection(generate_page.ConnectionString))
            {
                SqlCommand checkDuplicate = new SqlCommand(checkSelect, sqlConn);

                try
                {
                    checkDuplicate.Connection.Open();
                    SqlDataReader dataCheck = checkDuplicate.ExecuteReader();
                    if (dataCheck.Read() == true)
                    {
                        // if the page title exists set the flag to true
                        isDuplicateTitle = true;
                    }
                    else
                    {   
                        // else set it to false
                        isDuplicateTitle = false;
                    } 
                 
                }
                catch (Exception errMsg)
                {
                    // if SQL has other error, throw this message
                    exceptionGenerated = errMsg.Message;
                }
                finally
                {
                    // close the connection regardless if it's successful or not
                    sqlConn.Close();          
                }
            }


            // if it's not a duplicate, call a function that would create the page
            if (isDuplicateTitle == false)
            {
                try
                {
                    // try inserting the data using a try-catch block
                    // to catch any error thrown by the database
                    // I got the following code from christine
                    generate_page.InsertCommand = insertQuery;
                    generate_page.Insert();
                    //set the flag of page created to true
                    pageCreated = true;
                }
                catch (Exception errMsg)
                {
                    // catch any error and store it
                    exceptionGenerated = errMsg.Message;
                }
            }
            else
            {
                exceptionGenerated = String.Format("Sorry, {0} already exists!",titlePage);
            }

            // generate a link to the page
            if (pageCreated == true)
            {
                using (SqlConnection generateLink = new SqlConnection(generate_page.ConnectionString))
                {
                    SqlCommand createLink = new SqlCommand(retrieveMAX, generateLink);
                    try
                    {
                        createLink.Connection.Open();
                        SqlDataReader newLink = createLink.ExecuteReader();
                        // if the query has a result then generate the successful message
                        if (newLink.Read())
                        {
                            successMessage = String.Format("{0} was successfully created! <a href=\"Content-Page.aspx?pageid={1}\" target=\"_blank\">View the page</a>", titlePage, newLink[0]);
                        }
                    }
                    catch (Exception errMsg)
                    {
                        // catch and store the exception
                        exceptionGenerated = errMsg.Message;
                    }
                    finally
                    {
                        // close the connection whether it's successful or not
                        generateLink.Close();
                    }
                }
            }

            // output message if it catches an exception
            if (exceptionGenerated.Equals("") == false)
            {
                result.InnerHtml = String.Format("<p class=\"alert alert-danger\">{0}</p>", exceptionGenerated);
            }
            else
            {
                // else output success message
                result.InnerHtml = String.Format("<p class=\"alert alert-success alert-dismissable\"> {0} {1} </p>", successMessage, dismissButton);
            }

        }
    }
}