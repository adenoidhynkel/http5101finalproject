﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

namespace webApplicationDevelopmentFinal
{
    public partial class Update_Page : System.Web.UI.Page
    {

        public string getPageContent = "SELECT * from pages WHERE page_id={0}";
        public string checkSelect = "SELECT LOWER(page_title) FROM pages WHERE LOWER(page_title) = '{0}' AND page_id <> {1};";
        public string updateContent = "UPDATE pages SET publish_status = '{0}', page_title = '{1}', page_content = '{2}', last_edit_date = SYSDATETIME(), page_in_menu = {3} {4} WHERE page_id = {5}";
        public string deleteContent = "DELETE FROM pages WHERE page_id={0}";
        public string title;
        public string content;
        public string publishStatus;
        public string publishDate;
        public bool publishLater;
        public bool isDuplicateTitle;
        public bool inMenu;
        public bool updatePublishStatus;
        public string exceptionGenerated;
        public string successMessage;
        public string pageRedirect = "Default.aspx?pagetitle={0}&deleted=true";
        public string statusTxt = "Are you sure with this action?";
        public string dismissButton = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\"> &times;</span ></button>";
        public string backToHome = " <a href=\"Default.aspx\">Click Here to go back to pages</a>";
        public int menuFlag;


        // I took this from christine's code
        private string pageid
        {
            get { return Request.QueryString["pageid"]; }
        }



        // reference for storing sessions https://www.guru99.com/asp-net-session-management.html
        // I based this on Christine's query string code
        // I took this from christine's code

        // retrieves login status stored in session 
        private string login
        {
            // cast session to string data type
            get { return (string)Session["login"]; }
            set { Session["login"] = value; }
        }

        // retrieves username stored in session
        private string username
        {
            // cast session to string data type
            get { return (string)Session["username"]; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            exceptionGenerated = "";
            // check if it's a pageload or a postback
            // reference https://docs.microsoft.com/en-us/dotnet/api/system.web.ui.page.ispostback?view=netframework-4.7.2
            if (!Page.IsPostBack)
            {

                // check if user is logged in, otherwise redirect to login page
                if (login == "" || login == null)
                {
                    login = "failed";
                    Response.Redirect("Login-Form.aspx");
                }

                if (username != "" || username != null)
                {
                    editPageAuthor.Text = username;
                }

                // I took this if statement from christine's code
                if (pageid == "" || pageid == null)
                {
                    result.InnerHtml = "<p class=\"alert alert-danger\"> Failed to load user data, please go back and try again </p>";
                    return;
                }
                else
                {
                    getPageContent = String.Format(getPageContent, pageid);
                    using (SqlConnection loadValues = new SqlConnection(edit_page.ConnectionString))
                    {
                        SqlCommand checkValues = new SqlCommand(getPageContent, loadValues);

                        try
                        {
                            // try opening the connection
                            checkValues.Connection.Open();

                            // prepare the data reader
                            SqlDataReader getValues = checkValues.ExecuteReader();

                            if (getValues.Read())
                            {
                                title = getValues[1].ToString();
                                content = getValues[2].ToString();
                                publishLater = getValues[4].ToString().Equals("unpublished") == true ? true : false;
                                publishDate = getValues[5].ToString();
                                inMenu = getValues[7].ToString().Equals("True") == true ? true : false;                              
                            }
                            else
                            {
                                exceptionGenerated = "<p class=\"alert alert-danger\"> Failed to load user data, please go back and try again </p>";
                            }
                        }
                        catch (Exception ex)
                        {
                            exceptionGenerated = ex.Message;
                        }
                        finally
                        {
                            // close the connection
                            loadValues.Close();
                        }
                    }
                }

                if (exceptionGenerated.Equals("") == false)
                {
                    result.InnerHtml = exceptionGenerated;
                }
                else
                {
                    editPageTitle.Text = title;
                    editPageContent.Text = content;
                    editPageAuthor.Text = username;
                    // I got this code from Christine
                    btnDeletePage.OnClientClick = String.Format("if(!confirm('{0}')) return false", statusTxt);
                    if (publishLater == true)
                    {
                        // check the publish later if the status is unpublished
                        chkPublishLater.Checked = true;
                    }

                    if (inMenu == true)
                    {
                        // check the item in menu if the status is True
                        chkAddInMenu.Checked = true;
                    }
                }
            }
        }

        protected void Update_Data(object sender, EventArgs e)
        {
            if (pageid == "" || pageid != null)
            {
                if (editPageTitle.Text.Equals("") == false || editPageContent.Text.Equals("") == false)
                {

                    // check for server side validation
                    // search for existing page titles

                    // check against the pageid to make sure it does not conflict the page itself
                    checkSelect = String.Format(checkSelect, editPageTitle.Text, pageid);
                    using (SqlConnection checkDupe = new SqlConnection(edit_page.ConnectionString))
                    {
                        SqlCommand checkDuplicate = new SqlCommand(checkSelect, checkDupe);

                        try
                        {
                            checkDuplicate.Connection.Open();
                            SqlDataReader dataCheck = checkDuplicate.ExecuteReader();
                            if (dataCheck.Read() == true)
                            {
                                // if the page title exists set the flag to true
                                isDuplicateTitle = true;
                            }
                            else
                            {
                                // else set it to false
                                isDuplicateTitle = false;
                            }

                        }
                        catch (Exception errMsg)
                        {
                            // if SQL has other error, throw this message
                            exceptionGenerated = errMsg.Message;
                        }
                        finally
                        {
                            // close the connection regardless if it's successful or not
                            checkDupe.Close();
                        }
                    }

                    if (isDuplicateTitle == false)
                    {
                        title = editPageTitle.Text;
                        content = editPageContent.Text;
                        //escape the aphostrophe character
                        title = title.Replace("'", "''");
                        content = content.Replace("'", "''");

                        // if the user chose to unpublish
                        updatePublishStatus = chkPublishLater.Checked == true ? true : false;

                        // set the string to publish or unpublish 
                        publishStatus = updatePublishStatus == true ? "unpublished" : "published";

                        // if the add to menu checkbox was checked
                        menuFlag = chkAddInMenu.Checked == true ? 1 : 0;

                        // if the item is unpublished, and someone just edits it, do not update the publish date
                        // else publish it and update the publish date
                        if (publishDate == null && updatePublishStatus == false)
                        {
                            publishDate = ", publish_date = SYSDATETIME()";
                        }
                        else
                        {
                            publishDate = "";
                        }

                        // concatenate all the arguments via string format
                        updateContent = String.Format(updateContent, publishStatus, title, content, menuFlag, publishDate , pageid);

                        try
                        {
                            // insert the update
                            // I got the following code from christine
                            edit_page.UpdateCommand = updateContent;
                            edit_page.Update();
                            successMessage = String.Format("{0} was successfully edited! <a href=\"Content-Page.aspx?pageid={1}\" target=\"_blank\">View the page</a>", title, pageid);
                        }
                        catch (Exception ex)
                        {
                            exceptionGenerated = ex.Message;
                        }
                    }
                    else
                    {
                        exceptionGenerated = String.Format("Sorry! page title {0} already exists!", editPageTitle.Text);
                    }

                }
                else
                {
                    exceptionGenerated = "Fields cannot be empty!";
                }
            }

            if (exceptionGenerated.Equals("") == false)
            {
                result.InnerHtml = String.Format("<p class=\"alert alert-danger alert-dismissable\">{0} {1}</p>",exceptionGenerated, dismissButton);
            }
            else
            {
                result.InnerHtml = String.Format("<p class=\"alert alert-success alert-dismissable\"> {0} {1}</p>", successMessage, dismissButton);
            }
        }

        protected void Delete_Page(object sender, EventArgs e)
        {
            try
            {
                // concatenate values using string format
                deleteContent = String.Format(deleteContent, pageid);

                // I got this code from christine
                edit_page.DeleteCommand = deleteContent;
                edit_page.Delete();

                // redirect the page with Querystrings
                pageRedirect = String.Format(pageRedirect, editPageTitle.Text);
                Response.Redirect(pageRedirect);

            }
            catch (Exception ex)
            {
                exceptionGenerated = String.Format("<p class=\"alert alert-danger alert-dismissable\"> {0} {1} {2}</p>", ex.Message, backToHome, dismissButton);
                result.InnerHtml = exceptionGenerated;
            }
        }
    }
}