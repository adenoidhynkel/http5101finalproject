﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

namespace webApplicationDevelopmentFinal
{
    public partial class _Default : Page
    {

        // editor views only his/her posts
        public string getPublished = "SELECT page_id, page_title, page_content, publish_status, publish_date, " +
                                     "last_edit_date, users_login_name FROM pages p JOIN users u ON p.author_id = u.users_id " +
                                     "WHERE publish_status = 'published' AND users_id = {0};";

        public string getUnpublished = "SELECT page_id, page_title, page_content, publish_status, publish_date, " +
                                     "last_edit_date, users_login_name FROM pages p JOIN users u ON p.author_id = u.users_id " +
                                     "WHERE publish_status = 'unpublished' AND users_id = {0};";


        // admin view can see all posts
        public string getPublishedAdmin = "SELECT page_id, page_title, page_content, publish_status, publish_date, last_edit_date, " +
                                          "users_login_name FROM pages p JOIN users u ON p.author_id = u.users_id WHERE publish_status = 'published';";

        public string getUnpublishedAdmin = "SELECT page_id, page_title, page_content, publish_status, publish_date, last_edit_date, " +
                                        "users_login_name FROM pages p JOIN users u ON p.author_id = u.users_id WHERE publish_status = 'unpublished';";

        public string dismissButton = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\"> &times;</span ></button>";

        public string thePubQuery, theUnpubQuery;

        // I got this code from christine
        private string pagetitle
        {
            get { return Request.QueryString["pagetitle"]; }
        }

        private string deleted
        {
            get { return Request.QueryString["deleted"]; }
        }

        // reference for storing sessions https://www.guru99.com/asp-net-session-management.html
        // I based this on Christine's query string code

        // retrieves login status stored in session 
        private string login
        {
            // cast session to string data type
            get { return (string)Session["login"]; }
            set { Session["login"] = value; }
        }

        // retrieves userid stored in session
        private int userid
        {
            // cast session to int data type
            get { return (int)Session["userid"]; }
            set { Session["userid"] = value; }
        }        
        
        // retrieves roleid stored in session
        private int roleid
        {
            // cast session to int data type
            get { return (int)Session["roleid"]; }
            set { Session["roleid"] = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            // check if user is logged in, otherwise redirect to login page
            if (login == "" || login == null || roleid == 0 || userid == 0)
            {
                login = "failed";
                roleid = 0;
                userid = 0;
                Response.Redirect("Login-Form.aspx");
            }

            // check if the page was redirected after delete button was clicked
            if (pagetitle != ""  || pagetitle != null)
            {
                //check the second status
                if (deleted == "true" && deleted != null)
                {
                    result.InnerHtml = String.Format("<p class=\"alert alert-success alert-dismissable\">{0} was successfully deleted {1}</p>", pagetitle, dismissButton);
                }
            }


            //check the roleID if it's an admin or editor
            if (roleid == 1)
            {
                thePubQuery = getPublishedAdmin;
                theUnpubQuery = getUnpublishedAdmin;
            }
            else
            {
                thePubQuery = String.Format(getPublished, userid);
                theUnpubQuery = String.Format(getUnpublished, userid);
            }

            // Generate table for the published items
            Tuple<string, int> publishedPages = RetrievePages(thePubQuery);
            publishedContent.InnerHtml = publishedPages.Item1;
            publishedCount.InnerHtml = publishedPages.Item2.ToString();

            // Generate table for the unpublished items
            Tuple<string, int> unpublishedPages = RetrievePages(theUnpubQuery);
            unpublishedContent.InnerHtml = unpublishedPages.Item1;
            unpublishedCount.InnerHtml = unpublishedPages.Item2.ToString();
        }

        // I created this function because I will be generating the tables twice
        // one for published and another for unpublished
        // It will accept the query string and will return a Tuple data type

        // This is my first time using Tuple, from what I understand it is an array or 
        // like a List<type> that would allow me to store different data types like in this case
        // a string and an integer - the table data itself and the table data count
        // which I would use for the badges
        // here is my reference website for the tuples
        // https://www.codeproject.com/Articles/193537/C-4-Tuples

        protected Tuple<string,int> RetrievePages(string queryString)
        {
            string result = "";
            string tableResult = "";
            string datePublished = "";
            string lastUpdated = "";
            int count = 0;
            
            using (SqlConnection getPages = new SqlConnection(show_page.ConnectionString))
            {
                SqlCommand checkPages = new SqlCommand(queryString, getPages);

                try
                {
                    checkPages.Connection.Open();
                    SqlDataReader pageData = checkPages.ExecuteReader();
                    
                    //generate the table data

                    while (pageData.Read())
                    {
                        // I used ternary operation to assign the value of the last updated column
                        // if it's blank put N/A otherwise use the stored date
                        datePublished = pageData[4].ToString().Equals("") == true ? "N/A" : pageData[4].ToString();
                        lastUpdated = pageData[5].ToString().Equals("") == true ? "N/A" : pageData[5].ToString();

                        // start adding the data manually to the table similar to php
                        // using string.format 
                        // reference https://docs.microsoft.com/en-us/dotnet/api/system.string.format?view=netframework-4.7.2
                        result += "<tr>";
                        result += String.Format("<td><a class=\"btn btn-link\" href=\"Content-Page.aspx?pageid={0}\">{1}</td>", pageData[0].ToString(), pageData[1].ToString()); // Title Page link
                        result += String.Format("<td> {0} </td>", pageData[6].ToString()); // Author Name
                        result += String.Format("<td> {0} </td>", pageData[3].ToString()); // publish status
                        result += String.Format("<td> {0} </td>", datePublished); // publish date
                        result += String.Format("<td> {0} </td>", lastUpdated); // last updated date
                        result += String.Format("<td><a class=\"btn btn-link\" href=\"Update-Page.aspx?pageid={0}\">Edit</a></td>", pageData[0].ToString()); // edit page link
                        result += "</tr>";
                        // increase the count to display it later on the badge
                        count++;
                    }
                    
                }
                catch (Exception ex)
                {
                    result = ex.Message;
                }
                finally
                {
                    // close the connection regardless of the result
                    getPages.Close();
                }
            }

            // if no data was retrieved, return a message
            if (result.Equals(""))
            {
                result = "<tr><td colspan=\"6\" class=\"center\">No data to display</td></tr>";
            }

            // generate the table
            tableResult = generateTable(result);

            // assign the values in a tuple
            Tuple<string, int> pagesAndCount = Tuple.Create(tableResult, count);
            return pagesAndCount;

        }

        // this a simple string builder function to generate the data into a table
        // since asp does not allow to manipulate a table using .InnerHtml
        // that will force you to use DataGrids

        protected string generateTable(string tableData)
        {
            string tableHead, tableBody, completeTable;

            // table heading
            tableHead = "<thead>";
            tableHead += "<tr>";
            tableHead += "<th>Page Title</th>";
            tableHead += "<th>Page Author</th>";
            tableHead += "<th>Publish Status</th>";
            tableHead += "<th>Publish Date</th>";
            tableHead += "<th>Last Updated</th>";
            tableHead += "<th>Option</th>";
            tableHead += "</tr>";
            tableHead += "</thead>";

            // table body
            tableBody = String.Format("<tbody>{0}</tbody>", tableData);

            //assemble the table
            completeTable = String.Format("<table class=\"table table-striped\"> {0} {1} </table>", tableHead, tableBody);

            // return the table
            return completeTable;
        }
    }
}