﻿<%@ Page Title="Edit Menu" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Menu-Options.aspx.cs" Inherits="webApplicationDevelopmentFinal.Menu_Options" %>
<asp:Content ID="AssignMenu" ContentPlaceHolderID="MainContent" runat="server">
    <asp:SqlDataSource runat="server" id="get_pages" ConnectionString="<%$ ConnectionStrings:final_proj_db %>"></asp:SqlDataSource>
    <div class="col-md-6 col-xs-12">
        <h1><%: Page.Title %></h1>
         <div id="result" runat="server"></div>
        <div class="panel panel-default ">
            <div class="panel-heading">
                <h3 class="panel-title">Page List</h3>
            </div>
            <div class="panel-body">
                <div class="form-group chkbox">
                    <asp:CheckBoxList runat="server" ID="menuItemList" CssClass="chkbox"></asp:CheckBoxList>
                </div>
                <div class="form-group">
                    <asp:Button runat="server" ID="btnUpdateMenu" Text="Update Menu" class="btn btn-primary" OnClick="UpdateMenuItems"/>   
                </div>
            </div>
        </div>
    </div>
</asp:Content>
