﻿<%@ Page Title="Update Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Update-Page.aspx.cs" Inherits="webApplicationDevelopmentFinal.Update_Page" %>
<asp:Content ID="UpdatePage" ContentPlaceHolderID="MainContent" runat="server">
    <asp:SqlDataSource runat="server" id="edit_page" ConnectionString="<%$ ConnectionStrings:final_proj_db %>"></asp:SqlDataSource>
    <h1><%: Page.Title %></h1>
    <div id="result" runat="server"></div>
    <div class="row">
        <div class="col-md-8 col-xs-12">
          <fieldset>
            <legend>Fill in the details</legend>
            <div class="form-group">
                <asp:Label ID="lblPageTitle" AssociatedControlID="editPageTitle" runat="server">Page Title:</asp:Label>
            </div>
            <div class="form-group">
                <asp:TextBox ID="editPageTitle" runat="server" class="form-control" name="editTitle"></asp:TextBox>
                <asp:RequiredFieldValidator ID="editTitlePageVal" runat="server" ControlToValidate="editPageTitle" ErrorMessage="Please enter a title" ForeColor="Red"></asp:RequiredFieldValidator>
            </div>
            <div class="form-group">
                <asp:Label ID="lblPageContent" AssociatedControlID="editPageContent" runat="server">Content:</asp:Label>
                </div>
                <div class="form-group">
                <asp:TextBox ID="editPageContent" TextMode="MultiLine" Columns="350" Rows="10" runat="server" class="form-control"></asp:TextBox>
                <asp:RequiredFieldValidator ID="editPageContentVal" runat="server" ControlToValidate="editPageContent" ErrorMessage="Content cannot be empty" ForeColor="Red"></asp:RequiredFieldValidator> 
                </div>
          </fieldset>
        </div>
        <div class="col-md-4 col-xs-12">
            <fieldset>
                <legend>Options</legend>
                  <div class="form-group">
                      <asp:Label ID="lblPageAuthor" AssociatedControlID="editPageAuthor" runat="server">Author</asp:Label>
                  </div>
                  <div class="form-group">
                    <asp:TextBox ID="editPageAuthor" runat="server" class="form-control" disabled="true"></asp:TextBox>
                  </div>
                   <div class="form-group">
                    <asp:CheckBox ID="chkPublishLater" runat="server" />
                    <asp:Label ID="lblPublishLater" runat="server" AssociatedControlID="chkPublishLater">Unpublish Page</asp:Label>
                  </div>
                  <div class="form-group">
                    <asp:CheckBox ID="chkAddInMenu" runat="server" />
                    <asp:Label ID="lblAddInMenu" runat="server" AssociatedControlID="chkAddInMenu">Add to Main Menu</asp:Label>
                  </div>
                  <div class="form-group">
                    <asp:Button ID="btnEditePage" Class="btn btn-primary" type="submit" runat="server" Text="Update Page" OnClick="Update_Data"/>
                    <asp:Button ID="btnDeletePage" runat="server" Text="Delete Page" OnClick="Delete_Page" Class="btn btn-danger" />
                 </div>                  
            </fieldset>
        </div>
    </div>
</asp:Content>