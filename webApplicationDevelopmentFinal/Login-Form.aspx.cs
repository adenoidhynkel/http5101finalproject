﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

namespace webApplicationDevelopmentFinal
{
    public partial class Login_Form : System.Web.UI.Page
    {
        public string userN, userP, exceptionGenerated;
        public bool loginFlag;
        public string loginQuery = "SELECT users_id, users_login_name, users_role_id FROM users WHERE users_login_name = '{0}' AND users_login_pw = '{1}';";


        // reference for storing sessions https://www.guru99.com/asp-net-session-management.html
        // I based this on Christine's query string code

        // retrieves login status stored in session 
        private string login
        {
            // cast session to string data type
            get { return (string)Session["login"]; }
            set { Session["login"] = value; }
        }

        private string loggedout
        {
            // cast session to string data type
            get { return Request.QueryString["loggedout"]; }
        }
        // retrieves userid stored in session
        private int userid
        {
            // cast session to int data type
            get { return (int)Session["userid"]; }
            set { Session["userid"] = value; }
        }
        // retrieves username stored in session
        private string username
        {
            // cast session to string data type
            get { return (string)Session["username"]; }
        }

        // retrieves roleid stored in session
        private int roleid
        {
            // cast session to int data type
            get { return (int)Session["roleid"]; }
            set { Session["roleid"] = value; }
        }



        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                // check login session
                if (loggedout == "" || loggedout == null)
                {
                    return;
                }
                else if(loggedout.Equals("success") == true )
                {
                    // destroy session
                    // reference https://stackoverflow.com/questions/5330268/how-to-kill-a-session-or-session-id-asp-net-c
                    Session.Clear();
                    // set values from other pages
                    roleid = 0;
                    userid = 0;
                    login = "loggedout";
                }

                // check login status
                if (login == "" || login == null)
                {
                    return;
                }
                else if (login == "loggedout")
                {
                    result.InnerHtml = "<p class=\"alert alert-info\">You have logged out</p>";
                }
                else if (login == "failed")
                {
                    result.InnerHtml = "<p class=\"alert alert-danger\">You are not logged in!</p>";
                }
                else
                {
                    Response.Redirect("Default.aspx");
                }

            }
        }

        protected void Login_Check(object sender, EventArgs e)
        {
            userN = userName.Text;
            userP = userPassword.Text;
            exceptionGenerated = "";

            if (userN.Equals("") == false && userP.Equals("") == false)
            {
                loginQuery = String.Format(loginQuery, userN, userP);

                using (SqlConnection loginConn = new SqlConnection(login_page.ConnectionString))
                {
                    SqlCommand loginAttempt = new SqlCommand(loginQuery, loginConn);

                    // try opening the connection
                    try
                    {
                        loginAttempt.Connection.Open();
                        SqlDataReader checkLogin = loginAttempt.ExecuteReader();

                        if (checkLogin.Read() == true)
                        {
                            // store session
                            Session["userid"] = checkLogin[0];
                            Session["username"] = checkLogin[1];
                            Session["roleid"] = checkLogin[2];
                            Session["login"] = "success";
                            Response.Redirect("Default.aspx");
                        }
                        else
                        {
                            loginFlag = false;                           
                        }
                    }
                    catch(Exception ex)
                    {
                        // catch a MS SQL exception error
                        exceptionGenerated = ex.Message;
                    }
                    finally
                    {
                        // close the connection
                        loginConn.Close();
                    }

                    // if there was an error, we will not show the user the error from database
                    // instead we will tell them that there is something wrong with their credentials
                    if (exceptionGenerated.Equals("") == false)
                    {
                        // if it's a MSSQL ERROR
                        exceptionGenerated = "We encountered an unexpected Error, Please contact support.";
                    }
                    else if (loginFlag == false)
                    {
                        // If the credentials are incorrect
                        exceptionGenerated = "Your username or password is incorrect. Please try again.";
                    }

                    result.InnerHtml = String.Format("<p class=\"alert alert-danger\">{0}</p>", exceptionGenerated);
                }                
            }
            else
            {
         
            }           
        }
    }
}