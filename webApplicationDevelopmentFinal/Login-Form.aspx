﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Login-Form.aspx.cs" Inherits="webApplicationDevelopmentFinal.Login_Form" %>
<asp:Content ID="LoginForm" ContentPlaceHolderID="MainContent" runat="server">
    <asp:SqlDataSource runat="server" id="login_page" ConnectionString="<%$ ConnectionStrings:final_proj_db %>"></asp:SqlDataSource>
    <div class="col-md-4 col-md-push-4 col-xs-12">
        <h1>Login</h1>
        <div id="result" runat="server"></div>
        <div class="form-group">
            <asp:Label ID="lblUserName" AssociatedControlID="userName" runat="server">Username:</asp:Label>
        </div>
        <div class="form-group">
            <asp:TextBox ID="userName" runat="server" CssClass="form-control"></asp:TextBox>
            <asp:RequiredFieldValidator ID="userNVal" runat="server" ControlToValidate="userName" ErrorMessage="Please enter a user name" ForeColor="Red"></asp:RequiredFieldValidator>  
        </div>        
        <div class="form-group">
            <asp:Label ID="lblPassword" AssociatedControlID="userPassword" runat="server">Password:</asp:Label>
        </div>
        <div class="form-group">
            <!-- creating password field in asp https://stackoverflow.com/questions/8191702/c-sharp-password-textbox-in-a-asp-net-website -->
            <asp:TextBox ID="userPassword" TextMode="Password" runat="server" CssClass="form-control"></asp:TextBox>
            <asp:RequiredFieldValidator ID="passWordVal" runat="server" ControlToValidate="userPassword" ErrorMessage="Please enter your password!" ForeColor="Red"></asp:RequiredFieldValidator>  
        </div>        
        <div class="form-group">
            <asp:Button ID="btnLogin" CssClass="btn btn-primary pull-right" OnClick="Login_Check" runat="server" Text="Login"/>
        </div>
    </div>
</asp:Content>
