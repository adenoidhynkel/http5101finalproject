﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;


namespace webApplicationDevelopmentFinal
{
    public partial class Menu_Options : System.Web.UI.Page
    {
        public string menuQuery = "SELECT page_title, page_in_menu FROM pages where publish_status = 'published';";
        public string removeMenuQuery = "UPDATE pages SET page_in_menu = 0;";
        public string updateMenuQuery = "UPDATE pages SET page_in_menu = 1 WHERE page_title IN ({0});";
        public string exceptionGenerated = "";
        public string successMessage = "";

        private string login
        {
            // cast session to string data type
            get { return (string)Session["login"]; }
            set { Session["login"] = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            // check if user is logged in, otherwise redirect to login page
            if (login == "" || login == null)
            {
                login = "failed";
                Response.Redirect("Login-Form.aspx");
            }

            if (!Page.IsPostBack)
            {
                int count = 0;
                using (SqlConnection getPages = new SqlConnection(get_pages.ConnectionString))
                {
                    SqlCommand storePage = new SqlCommand(menuQuery, getPages);

                    // open the connection
                    try
                    {
                        storePage.Connection.Open();
                        SqlDataReader pageItem = storePage.ExecuteReader();

                        while (pageItem.Read())
                        {
                            menuItemList.Items.Add(pageItem[0].ToString());
                            if (pageItem[1].ToString().Equals("True") == true)
                            {
                                menuItemList.Items[count].Selected = true;
                            }

                            count++;
                        }
                    }
                    catch (Exception ex)
                    {
                        // catch exception for debugging
                        exceptionGenerated = ex.Message;
                    }
                    finally
                    {
                        // close the connection
                        getPages.Close();
                    }

                }

                if (exceptionGenerated.Equals("") == false)
                {
                    result.InnerHtml = String.Format("<p class=\"alert alert-danger\">{0}</p>", exceptionGenerated);
                }
            }

        }

        protected void UpdateMenuItems(object sender, EventArgs e)
        {
            string menuItems = "";
            // I retrieved this loop from my assignment 1
            for (int ctr = 0; ctr < menuItemList.Items.Count; ctr++)
            {
                if (menuItemList.Items[ctr].Selected == true)
                {
                    // add current value to variable so we can add it to our query later
                    // reference for manipulating the list items
                    // https://forums.asp.net/t/1315026.aspx?How%20can%20I%20set%20checkboxlist%20items%20as%20checked%20by%20default&fbclid=IwAR0BgGj2oOx349FtpBirGieErly1S61OJM9nC_cZ_Owb26AvRW7rfqcVRNQ
                    menuItems += String.Format("'{0}',", menuItemList.Items[ctr].Value);
                }
            }
            // remove the excess comma at the end of the string
            // reference: https://stackoverflow.com/questions/7901360/delete-last-char-of-string
            menuItems = menuItems.Remove(menuItems.Length - 1);
            updateMenuQuery = String.Format(updateMenuQuery, menuItems);

            // update the menu
            try
            {
                // I got this code from Christine

                // unassign all menus first
                get_pages.UpdateCommand = removeMenuQuery;
                get_pages.Update();

                // then assign all the checked values in the form
                // For now, I cannot control the order of the menu items for now as I lack time
                // i think jquery can do the updating the current order in the dom via drag and drop events

                get_pages.UpdateCommand = updateMenuQuery;
                get_pages.Update();

                successMessage = "You have successfully updated the menu! Please reload the page or <a href=\"Menu-Options.aspx\">Click here</a> to see the changes";
            }
            catch (Exception ex)
            {
                exceptionGenerated = ""+ex;
            }

            if (exceptionGenerated.Equals("") == false)
            {
                result.InnerHtml = String.Format("<p class=\"alert alert-danger\">{0}</p>", exceptionGenerated + updateMenuQuery);
            }
            else
            {
                result.InnerHtml = String.Format("<p class=\"alert alert-success\">{0}</p>", successMessage);
            }
        }
    }
}