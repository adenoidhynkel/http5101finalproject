﻿<%@ Page Title="Create a Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="create-page.aspx.cs" Inherits="webApplicationDevelopmentFinal.Create_Page" %>
<asp:Content ID="createPage" ContentPlaceHolderID="MainContent" runat="server">
    <asp:SqlDataSource runat="server" id="generate_page" ConnectionString="<%$ ConnectionStrings:final_proj_db %>"></asp:SqlDataSource>
    <h1><%: Page.Title %></h1>
    <div id="result" runat="server"></div>
    <div class="row">
        <div class="col-md-8 col-xs-12">
          <fieldset>
            <legend>Fill in the details</legend>
            <div class="form-group">
                <asp:Label ID="lblPageTitle" AssociatedControlID="pageTitle" runat="server">Page Title:</asp:Label>
            </div>
            <div class="form-group">
                <asp:TextBox ID="pageTitle" runat="server" class="form-control"></asp:TextBox>
                 <asp:RequiredFieldValidator ID="titlepageVal" runat="server" ControlToValidate="pageTitle" ErrorMessage="Please enter a title" ForeColor="Red"></asp:RequiredFieldValidator> 
            </div>
            <div class="form-group">
                <asp:Label ID="lblPageContent" AssociatedControlID="pageContent" runat="server">Content:</asp:Label>
                </div>
                <div class="form-group">
                <asp:TextBox ID="pageContent" TextMode="MultiLine" Columns="350" Rows="10" runat="server" class="form-control"></asp:TextBox>
                <asp:RequiredFieldValidator ID="pageContentVal" runat="server" ControlToValidate="pageContent" ErrorMessage="Content cannot be empty" ForeColor="Red"></asp:RequiredFieldValidator> 
                </div>
          </fieldset>
        </div>
        <div class="col-md-4 col-xs-12">
            <fieldset>
                <legend>Options</legend>
                  <div class="form-group">
                      <asp:Label ID="lblPageAuthor" AssociatedControlID="pageAuthor" runat="server">Author</asp:Label>
                  </div>
                  <div class="form-group">
                    <asp:TextBox ID="pageAuthor" runat="server" class="form-control" disabled="true"></asp:TextBox>
                  </div>
                  <div class="form-group">
                    <asp:CheckBox ID="chkPublishLater" runat="server" />
                    <asp:Label ID="lblPublishLater" runat="server" AssociatedControlID="chkPublishLater">Publish Later</asp:Label>
                  </div>                 
                <div class="form-group">
                    <asp:CheckBox ID="chkAddInMenu" runat="server" />
                    <asp:Label ID="lblAddInMenu" runat="server" AssociatedControlID="chkAddInMenu">Add to Main Menu</asp:Label>
                  </div>
                  <div class="form-group">
                    <asp:Button ID="btnCreatePage" Class="btn btn-primary" type="submit" runat="server" Text="Create Page" OnClick="Submit_Data"/>
                 </div>
            </fieldset>
        </div>
    </div>
</asp:Content>