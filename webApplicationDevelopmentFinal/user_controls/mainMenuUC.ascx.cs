﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

namespace webApplicationDevelopmentFinal.user_controls
{
    public partial class mainMenuUC : System.Web.UI.UserControl
    {
        public string menuQuery = "SELECT page_id, page_title FROM pages WHERE publish_status = 'published' AND page_in_menu = 1";
        public string dashMenu;

        // retrieves login status stored in session 
        private string login
        {
            // cast session to string data type
            get { return (string)Session["login"]; }
            set { Session["login"] = value; }
        }

        // retrieves username stored in session
        private string username
        {
            // cast session to string data type
            get { return (string)Session["username"]; }
            set { Session["username"] = value; }
        }

        
        protected void Page_Load(object sender, EventArgs e)
        {
            string mainMenu = "";
            string exceptionGenerated = "";

            using (SqlConnection generateMenu = new SqlConnection(generate_menu.ConnectionString))
            {
                SqlCommand getMenu = new SqlCommand(menuQuery, generateMenu);

                // open the connection

                try
                {
                    getMenu.Connection.Open();
                    // fetch data
                    SqlDataReader readMenu = getMenu.ExecuteReader();
                    // loop through the data
                    while (readMenu.Read())
                    {
                        mainMenu += String.Format("<li><a href=\"Content-Page.aspx?pageid={0}\">{1}</a></li>", readMenu[0], readMenu[1]);
                    }

                }
                catch (Exception ex)
                {
                    exceptionGenerated = ex.Message;
                }
                finally
                {
                    // close the connection
                    generateMenu.Close();
                }
            }

            // check for errors
            if (exceptionGenerated.Equals("") == false)
            {
                pageMainMenu.InnerHtml = exceptionGenerated;
            }
            else
            {
                if (mainMenu != null || mainMenu.Equals("") == false)
                {
                    pageMainMenu.InnerHtml = String.Format("<ul class=\"nav navbar-nav\">{0}</ul>", mainMenu);

                    // check if user is logged in, otherwise redirect to login page
                    if (login == "" || login == null || username == "" || username == null)
                    {
                        username = "Guest";
                    }

                    dashMenu += String.Format("<li class=\"dropdown\"><a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\">Hello, {0}! <span class=\"caret\"></span></a>", username);
                    dashMenu += "<ul class=\"dropdown-menu\">";
                    dashMenu += "<li><a href=\"Menu-Options.aspx\">Edit Menu</a></li>";
                    dashMenu += "<li><a href=\"#\">Edit Profile(Not Yet Implemented)</a></li>";
                    dashMenu += "<li><a href=\"#\">Edit Users(If user is admin, Not Yet Implemented)</a></li>";
                    dashMenu += "<li role=\"separator\" class=\"divider\"></li>";
                    dashMenu += "<li><a href=\"Login-Form.aspx?loggedout=success\">Logout</a></li>";
                    dashMenu = String.Format("<ul class=\"nav navbar-nav navbar-right\">{0}</ul></li></ul>", dashMenu);

                    pageMainMenu.InnerHtml += dashMenu;
                    
                }
            }
        }
    }
}