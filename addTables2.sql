﻿CREATE TABLE users (
	users_id INT NOT NULL IDENTITY,
	users_first_name VARCHAR(30),
	users_last_name VARCHAR(30),
	users_email_address VARCHAR(30),
	users_login_name VARCHAR(30),
	users_login_pw VARCHAR(100),
	users_role_id INT DEFAULT 2,
	PRIMARY KEY (users_id)
);

CREATE TABLE user_roles (
	user_role_id INT NOT NULL IDENTITY,
	user_role_desc VARCHAR(30)
);

CREATE TABLE pages (
page_id INT NOT NULL IDENTITY,
	page_title VARCHAR(50),
	page_content VARCHAR(MAX),
	author_id INT,
	publish_status VARCHAR(15) DEFAULT 'published',
	publish_date DATETIME DEFAULT SYSDATETIME(),
	last_edit_date DATETIME DEFAULT NULL,
	PRIMARY KEY(page_id)
);

ALTER TABLE pages 
ADD CONSTRAINT user_id_fk
FOREIGN KEY(author_id) REFERENCES users(users_id);

ALTER TABLE users
ADD CONSTRAINT users_role_fk 
FOREIGN KEY(users_role_id) REFERENCES user_roles(user_role_id);

INSERT INTO user_roles(user_role_desc) VALUES ('Administrator');
INSERT INTO users (users_first_name, users_last_name, users_email_address, users_login_name, users_login_pw, users_role_id)
VALUES ('Roentgen Mark', 'Martin', 'n01321102@humbermail.ca', 'adenoidhynkel', 'admin1234', 1);

SELECT * FROM pages;

SELECT * FROM users;


