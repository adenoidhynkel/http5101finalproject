﻿CREATE TABLE pages (
	page_id INT PRIMARY KEY,
	page_title VARCHAR(100) NOT NULL,
	page_content VARCHAR(MAX) NOT NULL
);